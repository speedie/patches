# suckless patches

This codeberg repository is for patches that I've (mostly) written.
All of them should be clean and follow the suckless patch guidelines!
Some may or may not require another patch on top, please read the description (at the top of the .diff).

Format: software-function-additional-function-version.diff

